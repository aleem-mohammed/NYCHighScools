//
//  NYCHighScoolsApp.swift
//  NYCHighScools
//
//  Created by Aleem on 14/03/23.
//

import SwiftUI

@main
struct NYCHighScoolsApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
